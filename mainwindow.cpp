#include "mainwindow.h"
#include "image-viewer.h"
#include "image-viewer-global.h"
#include "ui_mainwindow.h"
#include "storage.h"
#include "DirView.h"
#include <QSplitter>
#include <QListView>
#include <QLabel>
#include <QScrollArea>
#include <QImageReader>
#include <QFileDialog>
#include <QCommandLineParser>
#include <QFileSystemModel>
#include <QTreeView>
#include <QApplication>
#include <QFileIconProvider>
#include <QScreen>
#include <QCommandLineOption>
#include <QKeyEvent>
#include <QEvent>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    const QList<int> sizeList = {200, 500, 500};
    QSplitter *splitter = new QSplitter(parent);
    viewer = new pal::ImageViewer(this, false, true, true);
    viewer2  = new pal::ImageViewer(this, true, false, false);
    tree = new DirView(this);
    this->installEventFilter(this);

    store = new Storage();

    splitter->addWidget(tree);
    splitter->addWidget(viewer);
    splitter->addWidget(viewer2);
    splitter->setSizes(sizeList);

    QMenu *sessionMenu = menuBar()->addMenu(tr("&Session"));
    QAction *openAct = new QAction(tr("&Open..."), this);
    sessionMenu->addAction(openAct);
    connect(openAct, &QAction::triggered, this, &MainWindow::openSession);
    QAction *saveAct = new QAction(tr("&Save..."), this);
    sessionMenu->addAction(saveAct);
    connect(saveAct, &QAction::triggered, this, &MainWindow::saveSession);

    auto open_action = new QAction(tr("Open a directory..."), this);
    connect(open_action, &QAction::triggered, [=] {
        QString path = QFileDialog::getExistingDirectory(nullptr, "Open Camera root directory",
                                                    nullptr, nullptr);
        if (path.isEmpty())
            return;
        tree->setNewDir(path);
    });

    connect(tree, SIGNAL(directoryChanged(QStringList)), viewer2, SLOT(setCameras(QStringList)));
    connect(tree, SIGNAL(processedDirectoryChanged(QStringList)), viewer, SLOT(setProcessedDir(QStringList)));
    connect(tree, SIGNAL(setMainCameraImage(QString)), this, SLOT(loadMainCameraImage(QString)));
    connect(tree, SIGNAL(setPairedCameraImage(QString)), this, SLOT(loadPairedCameraImage(QString)));
    connect(this, SIGNAL(spacePressed()), this, SLOT(handleToggle()));
    connect(this, SIGNAL(enterPressed()), this, SLOT(handleToggle()));
    connect(viewer, SIGNAL(toggleProcessed(bool)), this, SLOT(handleProcessedSelection(bool)));
    connect(viewer2->cameraSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(refreshImages(int)));

    auto file_menu = menuBar()->addMenu(tr("&Open Directory"));
    file_menu->addAction(open_action);

    setCentralWidget(splitter);
    resize(1200, 800);
}

// Test for whether a file is valid
bool MainWindow::isFileValid(const QString &path)
{
    return QFileInfo::exists(path) && QFileInfo(path).isFile();
}

// Opens a session file. We will lock the directory to the session's file path
void MainWindow::openSession()
{
    QString path = QFileDialog::getOpenFileName(this);
    if (path.isEmpty())
        return;
    QString fixDirectory = store->readSessionFile(path);
    tree->setNewDir(fixDirectory);
}

// Saves a session file.
void MainWindow::saveSession()
{
    QString fileName = QFileDialog::getSaveFileName(this);
    if (!fileName.isEmpty())
    {
        bool success = store->writeSessionFile(fileName, tree->getCurrentDirectory());
        if (success == false)
        {
            QMessageBox msgBox;
            msgBox.setText("Not currently in any camera directory.");
            msgBox.setInformativeText(tree->getCurrentDirectory() + " is not a camera directory.\nPlease navigate to the camera directory that you wish to save to your session file.");
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();
        }
    }
}

void MainWindow::handleProcessedSelection(const bool toggle)
{
    if (toggle)
    {
        loadMainCameraProcessedImage(currentPath);
        loadPairedCameraProcessedImage(currentImageFilename);
    }
    else
    {
        refreshImages(0);
    }
}

// Check if the image pair should be toggled or not.
void MainWindow::handleToggle()
{
    bool added = store->toggleSelection(tree->getCurrentDirectory(), tree->getMainCamera(), viewer2->currentCameraSelection(), currentImageFilename);
    viewer->toggleSelection(added);
    viewer2->toggleSelection(added);
}

// Catch the enter key. We use enter key to select the image pair.
// We really need to use the space key.
bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type()==QEvent::KeyPress)
    {
        QKeyEvent *key = static_cast<QKeyEvent*>(event);
        if (key->key() == Qt::Key_Space)
        {
            // Do stuff with space
            emit spacePressed();
        }
        else if (key->key() == Qt::Key_Enter || key->key() == Qt::Key_Return)
        {
            emit enterPressed();
        }
        else
        {
            // No eye deer
        }
        return true;
    }
    else
    {
        return QObject::eventFilter(obj, event);
    }
    return false;
}

// Reload the images and check its toggle.
void MainWindow::refreshImages(const int)
{
    loadMainCameraImage(currentPath);
    loadPairedCameraImage(currentImageFilename);
    viewer->toggleSelection(store->checkSelection(tree->getCurrentDirectory(), tree->getMainCamera(), viewer2->currentCameraSelection(), currentImageFilename));
    viewer2->toggleSelection(store->checkSelection(tree->getCurrentDirectory(), tree->getMainCamera(), viewer2->currentCameraSelection(), currentImageFilename));
}

// Loads the processed image of the selected raw image for the left image
void MainWindow::loadMainCameraProcessedImage(const QString &path)
{
    QString processedPath = path.section('/', 0, -3) + '/' + viewer->getProcessedDirectory() + '/' + path.section('/', -2, -1);
    viewer->setImage(QImage(processedPath));
}

// Loads the processed image of the selected raw image for the right image
void MainWindow::loadPairedCameraProcessedImage(const QString &filename)
{
    QString cameraSelection = viewer2->currentCameraSelection();
    QString finalPath = tree->getCurrentDirectory() + '/' + viewer->getProcessedDirectory() + '/' + cameraSelection + '/' + filename;
    viewer2->setImage(QImage(finalPath));
}

// Loads the image for the left image.
void MainWindow::loadMainCameraImage(const QString &path)
{
    currentPath = path;
    viewer->setImage(QImage(path));
    // Check list to toggle
}

// Loads the image for the right image.
void MainWindow::loadPairedCameraImage(const QString &filename)
{
    currentImageFilename = filename;
    QString cameraSelection = viewer2->currentCameraSelection();
    QString finalPath = tree->getCurrentDirectory() + '/' + cameraSelection + '/' + filename;
    viewer2->setImage(QImage(finalPath));
    // Check list to toggle
    store->readSessions(tree->getCurrentDirectory());
    bool leftToggled = viewer->toggleSelection(store->checkSelection(tree->getCurrentDirectory(), tree->getMainCamera(), viewer2->currentCameraSelection(), currentImageFilename));
    bool rightToggled = viewer2->toggleSelection(store->checkSelection(tree->getCurrentDirectory(), tree->getMainCamera(), viewer2->currentCameraSelection(), currentImageFilename));

    if (leftToggled && rightToggled)
    {
        QString leftProcessed = tree->getCurrentDirectory() + '/' + viewer->getProcessedDirectory() + '/' + tree->getMainCamera() + '/' + currentImageFilename;
        QString rightProcessed = tree->getCurrentDirectory() + '/' + viewer->getProcessedDirectory() + '/' + viewer2->currentCameraSelection() + '/' + currentImageFilename;

        if (isFileValid(leftProcessed) && isFileValid(rightProcessed))
        {
            viewer->enableProcessed(Qt::Unchecked);
        }
    }
    else
    {
        viewer->disableProcessed();
    }

}

// We are ending the session. Write the files.
void MainWindow::closeEvent(QCloseEvent *event)
{
    // Write files
    store->writeAllFiles();
    event->accept();
}

MainWindow::~MainWindow()
{
    delete ui;
}

