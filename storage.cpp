#include "storage.h"
#include <QString>
#include <QFileInfo>
#include <QLinkedList>
#include <QTextStream>

#define DEFAULT_SESSION_SIZE 10

Storage::Storage()
{
}

bool Storage::isFileValid(const QString &path)
{
    return QFileInfo::exists(path) && QFileInfo(path).isFile();
}

/* This function takes in the rootpath of the camera folder, the 2 camera names and the image file name
 * It then searches the index and if it finds the same, it is removed and returns false.
 * If it cannot find an identical index, it adds this into the list and returns true.
 */
bool Storage::toggleSelection(const QString &root, const QString &camera1, const QString &camera2, const QString &image)
{
    // Locate the camera set
    QLinkedList<Cameralist*>::iterator i;
    for(i=sessions.begin(); i!=sessions.end(); i++)
    {
        if((*i)->cameraroot == root)
        {
            // Found
            break;
        }
    }

    // If we have the camera set, locate the camera pair
    QLinkedList<Camerapair*>::iterator j;
    QLinkedList<QString>::iterator k;
    if (i != sessions.end())
    {
        for(j=(*i)->pairlist.begin(); j!=(*i)->pairlist.end(); j++)
        {
            if(((*j)->camera1 == camera1 && (*j)->camera2 == camera2)
              ||((*j)->camera1 == camera2 && (*j)->camera2 == camera1))
            {
                break;
            }

        }
        if (j != (*i)->pairlist.end())
        {
            // Found the camera pair. Now locate file.
            k = std::find((*j)->filename.begin(), (*j)->filename.end(), image);
            if(k!=(*j)->filename.end())
            {
                // Found the file. Delete it.
                (*j)->filename.erase(k);
                return false;
            }

        }
    }

    // Can't find it? Insert it
    QString fullcamera1file = root + '/' + camera1 + '/' + image;
    QString fullcamera2file = root + '/' + camera2 + '/' + image;

    bool valid1 = isFileValid(fullcamera1file);
    bool valid2 = isFileValid(fullcamera2file);

    // We have a valid file, add it.
    if (valid1 && valid2)
    {
        bool createdCamera = false;
        // Camera root doesn't exist
        if(i==sessions.end())
        {
            Cameralist *camera = new Cameralist();
            camera->cameraroot = root;
            sessions.append(camera);
            i = std::find(sessions.begin(), sessions.end(), camera);
            createdCamera = true;
        }
        if(createdCamera || j==(*i)->pairlist.end())
        {
            Camerapair *pair = new Camerapair();
            pair->camera1 = camera1;
            pair->camera2 = camera2;
            (*i)->pairlist.append(pair);
            j = std::find((*i)->pairlist.begin(), (*i)->pairlist.end(), pair);
        }
        (*j)->filename.append(image);

        return true;
    }

    return false; // Can't find or did nothing.
}

bool Storage::checkSelection(const QString &root, const QString &camera1, const QString &camera2, const QString &image)
{
    QString fullcamera1file = root + '/' + camera1 + '/' + image;
    QString fullcamera2file = root + '/' + camera2 + '/' + image;

    bool valid1 = isFileValid(fullcamera1file);
    bool valid2 = isFileValid(fullcamera2file);

    if (valid1 && valid2)
    {
        // Locate the camera set
        QLinkedList<Cameralist*>::iterator i;
        for(i=sessions.begin(); i!=sessions.end(); i++)
        {
            if((*i)->cameraroot == root)
            {
                // Found
                // If we have the camera set, locate the camera pair
                QLinkedList<Camerapair*>::iterator j;
                QLinkedList<QString>::iterator k;
                for(j=(*i)->pairlist.begin(); j!=(*i)->pairlist.end(); j++)
                {
                    if(((*j)->camera1 == camera1 && (*j)->camera2 == camera2)
                      ||((*j)->camera1 == camera2 && (*j)->camera2 == camera1))
                    {
                        // Found the camera pair, now locate the file
                        k = std::find((*j)->filename.begin(), (*j)->filename.end(), image);
                        if(k!=(*j)->filename.end())
                        {
                            // Found the file
                            return true;
                        }
                    }
                }
            }
        }
    }

    return false;
}

void Storage::writeAllFiles()
{
    QFile *file = nullptr;
    QTextStream *out;

    // Locate the camera set
    QLinkedList<Cameralist*>::iterator i;
    for(i=sessions.begin(); i!=sessions.end(); i++)
    {
        QLinkedList<Camerapair*>::iterator j = (*i)->pairlist.begin();
        QLinkedList<QString>::iterator k;
        int counter = 1;
        for(int l=1; l<=(*i)->pairlist.size(); l++)
        {
            if((*j)->filename.size() > 0)
            {
                // There are files, write.
                if (file == nullptr)
                {
                    file = new QFile((*i)->cameraroot + "/calibrationlist");
                    file->open(QIODevice::WriteOnly | QIODevice::Text);
                    out = new QTextStream(file);
                    (*out) << "{";
                    (*out) << "\"path\" : \"" << (*i)->cameraroot << "\",";
                }
                (*out) << "\"pair" << counter++ << "\" : {";
                (*out) << "\"camera1name\" : \"" << (*j)->camera1 << "\",";
                (*out) << "\"camera2name\" : \"" << (*j)->camera2 << "\",";
                (*out) << "\"filelist\" : [";
                k = (*j)->filename.begin();
                for(int m=1; m<=(*j)->filename.size(); m++)
                {
                    if(m==(*j)->filename.size())
                    {
                        (*out) << "\"" << (*k) << "\"";
                    }
                    else
                    {
                        (*out) << "\"" << (*k) << "\",";
                    }
                    k++;
                }
                (*out) << "]";
            }
            if (l==(*i)->pairlist.size())
                (*out) << "}";
            else
                (*out) << "},";
            j++;
        }

        if (file != nullptr)
        {
            (*out) << "}";
            file->close();
            delete file;
            file = nullptr;
            delete out;
            out = nullptr;
        }
    }
    printf("does nothing\n");
}

bool Storage::writeSessionFile(const QString &filename, const QString &root)
{
    QFile *file = nullptr;
    QTextStream *out;

    // Locate the camera set
    QLinkedList<Cameralist*>::iterator i;
    for(i=sessions.begin(); i!=sessions.end(); i++)
    {
        if ((*i)->cameraroot == root || root.contains((*i)->cameraroot))
            break;
        continue;
    }

    if (i == sessions.end())
    {
        // No camera set found.
        return false;
    }

    QLinkedList<Camerapair*>::iterator j = (*i)->pairlist.begin();
    QLinkedList<QString>::iterator k;
    int counter = 1;
    for(int l=1; l<=(*i)->pairlist.size(); l++)
    {
        if((*j)->filename.size() > 0)
        {
            // There are files, write.
            if (file == nullptr)
            {
                file = new QFile(filename);
                file->open(QIODevice::WriteOnly | QIODevice::Text);
                out = new QTextStream(file);
                (*out) << "{";
                (*out) << "\"path\" : \"" << (*i)->cameraroot << "\",";
            }
            (*out) << "\"pair" << counter++ << "\" : {";
            (*out) << "\"camera1name\" : \"" << (*j)->camera1 << "\",";
            (*out) << "\"camera2name\" : \"" << (*j)->camera2 << "\",";
            (*out) << "\"filelist\" : [";
            k = (*j)->filename.begin();
            for(int m=1; m<=(*j)->filename.size(); m++)
            {
                if(m==(*j)->filename.size())
                {
                    (*out) << "\"" << (*k) << "\"";
                }
                else
                {
                    (*out) << "\"" << (*k) << "\",";
                }
                k++;
            }
            (*out) << "]";
        }
        if (l==(*i)->pairlist.size())
            (*out) << "}";
        else
            (*out) << "},";
        j++;
    }

    if (file != nullptr)
    {
        (*out) << "}";
        file->close();
        delete file;
        file = nullptr;
        delete out;
        out = nullptr;
    }
    return true;
}

QString getToken(QTextStream &ts)
{
    QString token;
    QChar ch;
    bool open = false;

    while (!ts.atEnd())
    {
        ts.skipWhiteSpace();
        ts >> ch;

        if (ch == '{')
        {
            // Start of statement
        }
        else if (ch == '"')
        {
            if (!open)
                open = true;
            else
                return token;
        }
        else if (ch == ':')
        {

        }
        else if (ch == '}')
        {

        }
        else if (ch == '[')
        {

        }
        else if (ch == ']')
        {

        }
        else if (ch == ',')
        {

        }
        else
            token += ch;
    }

    return "";
    // We may have to cater to an end of non-void function condition.
}

void Storage::deleteSession(const QString &root)
{
    QLinkedList<Cameralist*>::iterator i;
    QLinkedList<Camerapair*>::iterator j;

    for (i=sessions.begin(); i!=sessions.end(); i++)
    {
        if ((*i)->cameraroot != root)
            continue;
        else
            break;  // We found our candidate
    }

    for (j=(*i)->pairlist.begin(); j!=(*i)->pairlist.end(); j++)
    {
        (*j)->filename.clear();
    }
    (*i)->pairlist.clear();
    sessions.erase(i);
}

QString Storage::readSessionFile(const QString &root)
{
    QFile *file = new QFile(root);
    QString token;
    bool skip = false;

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
        return "";

    QTextStream ts(file);
    Cameralist *camera = new Cameralist();
    QLinkedList<Camerapair*>::iterator j;

    while (!ts.atEnd())
    {
        if (!skip)
            token = getToken(ts);
        skip = false;

        if (token == "path")
        {
            camera->cameraroot = getToken(ts);
        }
        else if (token.contains("pair"))
        {
            token = getToken(ts);
            Camerapair *pair = new Camerapair();
            if (token == "camera1name" || token == "camera2name")
            {
                pair->camera1 = getToken(ts);
                token = getToken(ts);
                pair->camera2 = getToken(ts);
                camera->pairlist.append(pair);
                j = std::find(camera->pairlist.begin(), camera->pairlist.end(), pair);
            }
        }
        else if (token == "filelist")
        {
            token = getToken(ts);
            while (!ts.atEnd() && token != "path" &&  token != "camera1name" && token != "camera2name" && !token.contains("pair") && token != "")
            {
                (*j)->filename.append(token);
                token = getToken(ts);
            }
            skip = true;
        }
        else
        {
            if (!ts.atEnd())
            {
                file->close();
                delete file;
                return "";
            }
        }
    }

    QString directoryname = camera->cameraroot;
    if (resumedSessions.indexOf(directoryname) >= 0)
        deleteSession(directoryname);
    resumedSessions.append(camera->cameraroot);
    sessions.append(camera);

    file->close();
    delete file;
    return camera->cameraroot;
}

void Storage::readSessions(const QString &root)
{
    int foundIndex = resumedSessions.indexOf(root);
    QString filename = root + "/calibrationlist";

    // It's already open
    if (foundIndex >= 0)
        return;

    readSessionFile(filename);
}
