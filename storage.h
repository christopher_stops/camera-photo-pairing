#ifndef STORAGE_H
#define STORAGE_H

#include <QString>
#include <QLinkedList>

/* We are using linked lists here.
 * We have a linked list of camera sets. These are stored in sessions.
 * Each camera set will then contain a list of camera pairs. These are the paired cameras
 * containing a list of image file names which will be used to calibrate that camera pair.
 */

// We are defining structs as classes.
class Camerapair
{
public:
    QString camera1;
    QString camera2;
    QLinkedList<QString> filename;
};

class Cameralist
{
public:
    QString cameraroot;
    QLinkedList<Camerapair*> pairlist;
};

class Storage
{
public:
    Storage();
    // You give it a filename, the 2 cameras and the root path, it searches its database and returns if this is added
    // or removed.
    bool toggleSelection(const QString &root, const QString &camera1, const QString &camera2, const QString &image);
    bool checkSelection(const QString &root, const QString &camera1, const QString &camera2, const QString &image);
    void writeAllFiles();
    bool writeSessionFile(const QString &filename, const QString &root);
    QString readSessionFile(const QString &root);
    void readSessions(const QString &root);
    void deleteSession(const QString &root);

protected:
    bool isFileValid(const QString &path);

private:
    QLinkedList<Cameralist*> sessions;
    QList<QString> resumedSessions;
};

#endif // STORAGE_H
