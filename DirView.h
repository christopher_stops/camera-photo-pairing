#ifndef DIRVIEW_H
#define DIRVIEW_H

//#include <QFrame>
#include <QFileSystemModel>
#include <QTreeView>

class DirView : public QFrame
{
    Q_OBJECT

public:
    DirView(QWidget *parent);
    void setNewDir(const QString &path);
    QString getCurrentDirectory();
    QString getMainCamera();

//    ~DirView();

protected slots:
    void getImageFilenames(const QModelIndex &newindex, const QModelIndex &oldindex);
    void listCameraDirectories(const QModelIndex &index, const QString excluded = nullptr);
    void toggleSelection();

signals:
    void setMainCameraImage(const QString &path);
    void setPairedCameraImage(const QString &path);
    void directoryChanged(const QStringList &directoryList);
    void processedDirectoryChanged(const QStringList &processedDirectoryList);

private:
    //Ui::MainWindow *ui;
    QFileSystemModel *model;
    QTreeView *tree;
    QString rootPath;       // Cached root directory
    QString currentPath;    // Cached current directory you have selected
    QItemSelectionModel *sm;
    QStringList cameraDirectoryList;    // Cached list of cameras in your directory
    QString currentCamera1;     // Cached current camera selected
    QString currentImageFile;   // Cached current image file selected
    QModelIndex currentIndex;   // Cached selection index in the directory(image file usually)
    QStringList processedDirectoryName;
};

#endif // DIRVIEW_H
