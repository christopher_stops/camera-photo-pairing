#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "DirView.h"
#include "image-viewer-global.h"
#include "image-viewer.h"
#include "storage.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void loadMainCameraImage(const QString &path);
    void loadPairedCameraImage(const QString &filename);
    void loadMainCameraProcessedImage(const QString &path);
    void loadPairedCameraProcessedImage(const QString &filename);
    void handleToggle();
    void refreshImages(const int);
    void handleProcessedSelection(const bool toggle);

signals:
    void spacePressed();
    void enterPressed();

protected:
    bool eventFilter(QObject *watched, QEvent *event);
    void closeEvent(QCloseEvent *event);
    void openSession();
    void saveSession();

private:
    bool isFileValid(const QString &path);

    pal::ImageViewer *viewer;   // Left image viewing window
    pal::ImageViewer *viewer2;  // Right image viewing window
    DirView *tree;              // File/Directory browser
    Ui::MainWindow *ui;
    QString currentPath;        // Cached current directory
    QString currentImageFilename;   // Cached image being displayed/selected
    Storage *store;             // The file storage object
};
#endif // MAINWINDOW_H
