#include "DirView.h"

#include <QFileSystemModel>
#include <QTreeView>
#include <QDir>
#include <QVBoxLayout>

DirView::DirView(QWidget *parent)
    : QFrame(parent)
{
    const QString rootPath = QString();
    int32_t columnCount = 1;

    model = new QFileSystemModel(this);
    model->setRootPath("");

    tree = new QTreeView(this);
    tree->setModel(model);
    const QModelIndex rootIndex = model->index(QDir::cleanPath(rootPath));
    if (rootIndex.isValid())
        tree->setRootIndex(rootIndex);
    columnCount = model->columnCount(rootIndex);

    for (int i = 1; i < columnCount; i++)
        tree->setColumnHidden(i, true);
    tree->setAnimated(false);
    tree->setIndentation(20);
    tree->setSortingEnabled(true);
    tree->setSelectionMode(QAbstractItemView::SingleSelection);
    tree->setSelectionBehavior(QAbstractItemView::SelectRows);
    tree->setWindowTitle(QObject::tr("Dir View"));

    sm = tree->selectionModel();
    connect(sm, SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
            this, SLOT(getImageFilenames(QModelIndex, QModelIndex)));

    auto box = new QVBoxLayout;
    box->setContentsMargins(5,0,5,0);
    box->addWidget(tree, 1);
    setLayout(box);
}

// Setting the root directory for the browser tree to start on.
void DirView::setNewDir(const QString &path)
{
    model->setRootPath(path);
    if (!path.isEmpty()) {
        const QModelIndex rootIndex = model->index(QDir::cleanPath(path));
        if (rootIndex.isValid())
            tree->setRootIndex(rootIndex);
    }
    rootPath = path;
}

// Returns the value of the main camera selected on the browser.
QString DirView::getMainCamera()
{
    return currentCamera1;
}

// Obtain the filename of the selected entry.
void DirView::getImageFilenames(const QModelIndex &newindex, const QModelIndex &oldindex)
{
    QString mainCamera = model->filePath(newindex);
    if (model->isDir(newindex))
    {
        listCameraDirectories(newindex);
    }
    else
    {
        listCameraDirectories(newindex, mainCamera.section('/', -2, -2));
    }
    QString pairedCamera = model->fileName(newindex);
    currentCamera1 = mainCamera.section('/', -2, -2);
    currentImageFile = pairedCamera;
    currentIndex = newindex;
    emit setMainCameraImage(mainCamera);
    emit setPairedCameraImage(pairedCamera);

}

// Not needed yet
void DirView::toggleSelection()
{
    // Toggle this.
}

// Returns the current selected directory
QString DirView::getCurrentDirectory()
{
    return currentPath;
}

// Obtains the list of cameras located within this directory
void DirView::listCameraDirectories(const QModelIndex &index, const QString excluded)
{
    QModelIndex newindex;
    QString path;
    QStringList directoryList;
    QStringList processedDirectoryList;
    // This is a directory
    if (model->isDir(index))
    {
        newindex = index;
    }
    // This is a photo, we go to the root of this file's directory and parse.
    else
    {
        newindex = model->parent(index).parent();
    }
    path = model->filePath(newindex);
    QFileSystemModel currentDirectory;

    currentDirectory.setRootPath(path);
    currentPath = path;

    QDirIterator it(path, QDir::NoDotAndDotDot | QDir::Dirs, QDirIterator::NoIteratorFlags);
    while (it.hasNext())
    {
        QString dumb = it.next();
        QString debugging = dumb.section('/', -1);
        if (debugging != excluded)
        {
            if (debugging.contains("processed", Qt::CaseInsensitive))
                processedDirectoryList.append(debugging);
            else
                directoryList.append(debugging);
        }
    }
    if (processedDirectoryList != processedDirectoryName)
    {
        processedDirectoryName = processedDirectoryList;
        emit processedDirectoryChanged(processedDirectoryList);
    }
    if (directoryList != cameraDirectoryList)
    {
        cameraDirectoryList = directoryList;
        emit directoryChanged(directoryList);
    }
}
